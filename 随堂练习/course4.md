# 随堂练习4:内存表

## 设置In-Memory内存大小

```text
【示例5-1】启动数据库并观察SGA分配
$ sqlplus /  as sysdba
SQL> show parameter sga_

【示例5-2】设置In-Memory内存大小
本例设置inmemory_size为150m，如果设置内存允许，可以设置得更大一些。参数inmemory_max_populate_servers控制In-Memory后台工作者进程Wnnn的个数，进程数越多，装载速度越快，但消耗的资源也更多。
$ sqlplus /  as sysdba
SQL> ALTER SYSTEM SET inmemory_max_populate_servers=2 SCOPE=SPFILE;
SQL> ALTER SYSTEM SET inmemory_size=150m SCOPE=SPFILE;
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP
SQL> show parameter inmem
```

## 创建表employees的复制表employees_bak

```sql
$ sqlplus hr/123@localhost/pdborcl
SQL> create table employees_bak as select * from employees;
create table employees_bak as select * from employees;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
insert into employees_bak select * from employees_bak;
select count(*) from employees_bak;
876544行
```

## 设置hr用户有autotrace权限

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
SQL> @$ORACLE_HOME/sqlplus/admin/plustrce.sql
SQL> create role plustrace;
SQL> GRANT SELECT ON v_$sesstat TO plustrace;
SQL> GRANT SELECT ON v_$statname TO plustrace;
SQL> GRANT SELECT ON v_$mystat TO plustrace;
SQL> GRANT plustrace TO dba WITH ADMIN OPTION;
SQL> set echo off
SQL> GRANT plustrace TO hr;
```

## 设置表employees_bak为In-Memory列存储表

设置表employees_bak为In-Memory列存储表，并通过执行计划观察查询效率的提高。首先观察没有将employees_bak表设置为In-Memory时的执行计划，以hr用户登录,执行以下语句，对比in-memory前后的查询效率的变化

- 设置为列存储之前,普通方式：

```sql
$ sqlplus hr/123@localhost/pdborcl
SQL> SET AUTOTRACE ON
SQL> SELECT * FROM employees_bak where salary =9000;
```

- 跟踪结果是

```text
TABLE ACCESS FULL| EMPLOYEES_BAK
统计信息
----------------------------------------------------------
	  0  recursive calls
	  0  db block gets
       9083  consistent gets
	  0  physical reads
	  0  redo size
       1589  bytes sent via SQL*Net to client
	607  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  4  rows processed
```

- 设置为列存储方式:

```sql
SQL> ALTER TABLE employees_bak INMEMORY;

--第一次执行：employees_bak，才会被装载到In-Memory内存中。这是因为SALES的In-Memory优先级为默认的NONE
SQL> SELECT * FROM employees_bak where salary =9000;

--第二次执行：第一次执行完成后表才会被装载到In-Memory内存中。
SQL> SELECT * FROM employees_bak where salary =9000;
```

- 跟踪结果是

```text
TABLE ACCESS INMEMORY FULL| EMPLOYEES_BAK 
统计信息
----------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
       1589  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  4  rows processed

```

## 对比分析

- 普通方式：
  - TABLE ACCESS FULL 全表搜索
  - 9083  consistent gets
- in-memory方式：
  - TABLE ACCESS INMEMORY FULL 列存储搜索
  - 10  consistent gets
- 结论：in-memory方式查询速度快得多

## 实验完成后，删除表employees_bak

```sql
SQL>drop table employees_bak;
```
